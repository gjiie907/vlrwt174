#第一财经JJ游戏商人ctb
#### 介绍
JJ游戏商人【溦:844825】，JJ游戏商人【溦:844825】，	17、以后少惦记感同身受这回事，没人理解，就自己理解自己，压力大就去把所有事情努力做好。
我只是记住，我几天前在路上遇见了她，她没有看到我，我只是砸了她，心里没有浪潮。那种一点情绪让我突然意识到了。世界上的寒冷就像孩子的滚刀，这是一个习惯。
如今已是人到中年，没有了“少年不知愁滋味，为赋新词强说愁”的年少轻狂。有的是：失意时，“举杯浇愁愁更愁”的落寞，无奈时“只恐双溪舴艋舟，载不动许多愁”的感慨。

#### 参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
#### 特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/